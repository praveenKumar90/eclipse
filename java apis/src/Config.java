import java.sql.Connection;
import java.sql.DriverManager;

public class Config 
{
	Connection con = null;
	String Username =  "";
	String Password = "";
	String dbName = "";
	String host = "";
	String url = "";
	
	public Config(String host, String username, String password, String dbname) 
	{
		Username = username;
		Password = password;
		dbName = dbname;
		this.host = host;
		url = "jdbc:mysql://"+this.host+"/"+dbName;
	}
	
	public Connection connect()
	{
		try{  
			Class.forName("com.mysql.cj.jdbc.Driver");  
			con=DriverManager.getConnection(url, Username, Password);  
		}catch(Exception e){ System.out.println(e);}  
		return con;
	}
}
