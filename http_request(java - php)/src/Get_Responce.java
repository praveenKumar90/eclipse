import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Get_Responce 
{
	//hindi character from 2309 - 2360
	@SuppressWarnings("unchecked")
	public static void getResponce() throws Exception 
	{
		HttpURLConnectionExample obj = new HttpURLConnectionExample();
        String url = "http://localhost/backendforjava/get_patwari.php";
        System.setProperty("file.encoding", "utf-8");
        String responce = obj.sendGET(url);
        Object data = new JSONParser().parse(responce);
        JSONObject jsonObj = (JSONObject) data;
        jsonObj.keySet().forEach(keyStr ->{
            Object keyvalue = jsonObj.get(keyStr);
            System.out.println(keyStr+" = "+keyvalue);
        });
	}
	public static void main(String[] args) throws Exception 
	{
		getResponce();
	}
}
