package com.java.swing.update;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JTextField;
import javax.swing.text.Document;

@SuppressWarnings("serial")
public class UTextField extends JTextField {
    private String placeholder;
    private Color color;
    public UTextField() {
    	color = getDisabledTextColor();
    }

    
    public void setPlaceHolderColor(Color color) {
    	this.color = color;
    }
    
    public Color getPlaceHolderColor() {
    	return this.color;
    }
    
    public UTextField(
        final Document pDoc,
        final String pText,
        final int pColumns)
    {
        super(pDoc, pText, pColumns);
    }

    public UTextField(final int pColumns) {
        super(pColumns);
    }

    public UTextField(final String pText) {
        super(pText);
    }

    public UTextField(final String pText, final int pColumns) {
        super(pText, pColumns);
    }

    public String getPlaceholder() {
        return placeholder;
    }

    @Override
    protected void paintComponent(final Graphics pG) {
        super.paintComponent(pG);

        if (placeholder == null || placeholder.length() == 0 || getText().length() > 0) {
            return;
        }

        final Graphics2D g = (Graphics2D) pG;
        g.setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(this.color);
        g.drawString(placeholder, getInsets().left, pG.getFontMetrics()
            .getMaxAscent() + getInsets().top);
    }

    public void setPlaceholder(final String s) {
        placeholder = s;
    }

}