import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import com.java.swing.update.*;

public class Login extends JFrame
{
	private static final long serialVersionUID = 1L;
	UTextField uname = null;
	JPasswordField password = null;
	public Login()
	{
		super("bugs.rbaas.in");
		setSize(new Dimension(700, 500));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setDefaultLookAndFeelDecorated(true);
		setLayout(null);
		BufferedImage img = null;
		try {
		    img = ImageIO.read(new File("images/bg.jpg"));
		} catch (IOException e) {}
		Image dimg = img.getScaledInstance(1366, 768, Image.SCALE_SMOOTH);
		ImageIcon imageIcon = new ImageIcon(dimg);
		setContentPane(new JLabel(imageIcon));
		
		JLabel label = new JLabel("संयुक्त बग रिपोर्टिंग");
		label.setFont(new Font("", 1, 30));
		label.setForeground(Color.BLUE);
		label.setBounds(1366/2-100, 100, 500, 50);
		add(label);
		
		JPanel panel = new JPanel(null);
		panel.setBounds(1366/2-100, 750/2-300/2, 350, 200);
		uname = new UTextField();
		uname.setBounds(10, 10, 300, 25);
		uname.setFont(new Font("Times New Roman", 0, 16));
		uname.setPlaceHolderColor(Color.GRAY);
		uname.setPlaceholder("Username *");
		password = new JPasswordField();
		password.setBounds(10, 40, 300, 25);
		password.setToolTipText("Password *");
		panel.add(uname, BorderLayout.NORTH);
		panel.add(password, BorderLayout.SOUTH);
		add(panel, BorderLayout.CENTER);
	}
}