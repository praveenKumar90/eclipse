

public class Data 
{
	public static String data[][] = {
			{"ID", "NAME", "POST", "DOB"},	//HEADERS
			
			{"1", "JOHN", "MANAGER", "03/09/1992"},
			{"2", "JOEY", "SUPPORT", "03/06/1998"},
			{"3", "JHAKE ROEL", "ACCOUNTANT", "15/11/1987"},
			{"4", "MOSS", "DEVELOPER", "16/02/1982"},
			{"5", "ARMOND", "DIRECTOR", "15/09/1996"},
			{"6", "PRINCE", "HOD", "12/08/1995"},
			{"7", "JANNIE", "PROJECT MANAGER", "25/08/1995"},
			{"8", "NELSON", "DEVELOPER", "15/12/1997"},
			{"9", "PETERSEN", "DEVELOPER", "12/11/1995"}
	};
}
