package tableReader;

public class CreateTable 
{
	public static String getTable(String arg[][])
	{
		String data[][] = formArray(arg);
		int space[] = getMaxSizeString(data);
		String str = "";
		for(int x=0; x<data[0].length; x++){
			for(int y=0; y<space[x]; y++) {
				str += "-";
			}
			str+="+";
		}
		str += "\n";
		for(int i=0; i<data.length; i++) {
			for(int j=0; j<data[i].length; j++) {
				str+=data[i][j];
				str += "|";
			}
			str+="\n";
			for(int x=0; x<data[0].length; x++){
				for(int y=0; y<space[x]; y++) {
					str += "-";
				}
				str+="+";
			}
			str += "\n";
			
		}
		return(str);
	}
	
	public static int[] getMaxSizeString(String data[][])
	{
		int sData[] = new int[4];
		int size = 0;
		for(int i=0; i<data[0].length; i++) {
			for(int j=0; j<data.length; j++) {
				if(data[j][i].length()>size) {
					size = data[j][i].length();
					sData[i] = size;
				}
			}
			size = 0;
		}
		return sData;
	}
	
	public static String[][] formArray(String data[][]) 
	{
		int space[] = getMaxSizeString(data);
		String array[][] = data;
		String value;
		for(int i=0; i<data.length; i++) {
			for(int j=0; j<data[i].length; j++) {
				value = data[i][j];
				for(int s=0; s<space[j]-array[i][j].length()+3; s++) {
					value+=" ";
				}
				array[i][j] = value;
				value = "";
			}
		}
		return array;
	}
}
