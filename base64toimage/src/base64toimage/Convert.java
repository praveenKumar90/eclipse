package base64toimage;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

public class Convert 
{
	public static BufferedImage decodeToImage(String imageString) {
		 
        BufferedImage image = null;
        byte[] imageByte;
        try {
            BASE64Decoder  decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }
	
//	public static void main(String arg[]) throws IOException {
//		encoder();
//	}
//	
//	public static void encoder() throws IOException
//	{
//		BufferedImage image = ImageIO.read(new File("favicon.png"));
//		for(int i=0; i<image.getWidth(); i++) {
//			for(int j=0; j<image.getHeight(); j++) {
//				System.out.print(image.getRGB(i, j));
//			}
//			System.out.println();
//		}
//		BASE64Encoder encode = new BASE64Encoder();
//	}
}
